package com.mycompany.poliformismo;
public class ModeloBarato extends Marca{
    private int año;
    public ModeloBarato(String nombre, double precio,String fabricante, int año){
        super(nombre,precio,fabricante);
        this.año=año;   
    }
    public int getAño() {
        return año;
    }
    @Override
    public String imprimir(){
        return "nombre del producto: " + nombre +"\nprecio dle producto: "+ precio + "\nnombre del fabricante: " + fabricante + "\nAño de modelo: " + año;
    }
}
