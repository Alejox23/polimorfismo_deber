package com.mycompany.poliformismo;
public class ModeloCaro extends Marca{
    private String gama;
    public ModeloCaro(String nombre, double precio,String fabricante, String gama){
        super(nombre,precio,fabricante);
        this.gama=gama;        
}
    public String getGama() {
        return gama;
    }
    @Override
    public String imprimir(){
        return "nombre del producto: " + nombre +"\nprecio dle producto: "+ precio + "\nnombre del fabricante: " + fabricante + "\nTipo de gama: " + gama;
    }
}